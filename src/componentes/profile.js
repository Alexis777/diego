import React from "react";
import styled from "styled-components";
import {Flex, Sombra} from "./ui";
import {Controller, Scene} from "react-scrollmagic";



const Profilecont = styled.section`
     width:100%;
     min-height:90vh;
     position:relative;
     width:100%;
     background-image:url(./img/fondobody.jpg);
     background-position:center;
     background-repeat:no-repeat;  
     background-size:cover;
     background-attachment:fixed;  
     z-index:0;
`

const Profile = () => {
    return(
        <Profilecont id={"perfil"} >
            <Sombra aplpha={".7"}/>
            <div className={"col-12 col-md-8 mx-auto py-5"} >
            <Flex className={"col-12 position-relative "}>
                <div className="col-12 col-md-6 texto-p">
                    <h1 className={"cr"}>Diego Alejandro</h1>
                    <p className={"cb"}>
                        Cantante nacido en Medellín, apasionado por la Producción de cine y televisión, comenzó su carrera musical formando parte de la red de escuelas y bandas de la ciudad de Medellín donde estudió trombón y lectura musical, estudio piano y canto lírico en la Universidad de Antioquia y en Bellas Artes, en esta última hizo parte del coro Santa Cecilia donde fue tenor solista 6 años bajo la dirección del maestro Harry Hastwood. <br/>
                        <br/>Su carrera ha sido supervisada y guiada por los más prestigiosos maestros del país. <br/>
                        En el año 2014 fue ganador de dos medallas de Oro y un galardon al mejor cantante en la categoría canto lírico en The World Championships Performing Arts que se celebra en Hollywood California, después de su triunfo en Estados Unidos, comenzo una gira por Africa y Europa donde estuvo por 9 meses cantando y dando a conocer su talento.
                    </p>
                </div>

                <div className="col-12 col-md-6 foto-p px-0">
                    <img src="/img/diego1.jpeg" alt="" width={"100%"}/>
                </div>
            </Flex>
            </div>
        </Profilecont>
    )
}

export default Profile;