import React from "react";
import styled from "styled-components";
import {Flex} from "./ui";





const Menucont = styled(Flex)`

        width:auto;
        height:auto;

        position:fixed;
        top:50%;
        transform:translateY(-50%);
        right:calc((100vw - 80vw) / 2);
        flex-flow:column wrap;
        z-index:9999;
        align-items:flex-end;
        
        button{
            color:white;
            font-family: 'Staatliches', cursive;
            font-size:25px;
            cursor:pointer;
            padding: .3em 0;
            
            &:focus{
            outline:none;
            }
            
            &:hover{
            
            text-decoration:underline;
            }
        }
        
        .subir{
            cursor:pointer;
            display:block;
            padding:.5em 1em;
            min-width:150px;
        }
         
`
const Menu = ({alturas,accion,state}) => {

    return(


        <Menucont className={"d-none d-md-flex"}>
              <Flex column alg={"flex-end"} className="cont-menu menu">
                  <button onClick={() => accion(0)}>Inicio</button>
                  <button onClick={() => accion(document.getElementById('perfil').offsetTop)}>SOBRE MÍ</button>
                  <button onClick={() => accion(document.getElementById('discos').offsetTop)}>DISCOGRAFÍA</button>
                  <button onClick={() => accion(document.getElementById('04').offsetTop)}>EVENTOS</button>
                  <button onClick={() => accion(document.getElementById('05').offsetTop)}>GALERÍA</button>
                  <button onClick={() => accion(document.getElementById('pie').offsetTop)}>CONTACTO</button>
              </Flex>

        </Menucont>
    )
}

export default Menu;