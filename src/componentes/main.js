import {useState} from "react";
import Menu from "./menu";
import Inicio from "./inicio";
import Discografia from "./discografia";
import Profile from "./profile";
import Eventos from "./eventos";
import SliderFotos from "./slierfotos";
import Footer from "./footer";
import React from "react";
import {useSpring, animated} from 'react-spring';
import {useTransition} from 'react-spring'


 const Main = ({alturas,accion,Imagenes,cargando}) => {



     const [state,set] = useState(0)

     const props = useSpring({ scroll: state, from: { scroll: 0 } })

     const transitions = useTransition(cargando, null, {
         from: { position: 'absolute', opacity: 1, },
         enter: { opacity: 1 },
         leave: { opacity: 0 },
     })



    return(

      <animated.div scrollTop={props.scroll} className={"App"}>
          {transitions.map(({ item, key, props }) =>
              item &&
              <animated.div className="cargando" style={props} key="key">
                  <div className={"py-4"}>
                      <p>--- Diego Alejandro.co ---</p>
                  </div>
                  <div id="fountainG">
                      <div id="fountainG_1" className="fountainG"></div>
                      <div id="fountainG_2" className="fountainG"></div>
                      <div id="fountainG_3" className="fountainG"></div>
                      <div id="fountainG_4" className="fountainG"></div>
                      <div id="fountainG_5" className="fountainG"></div>
                      <div id="fountainG_6" className="fountainG"></div>
                      <div id="fountainG_7" className="fountainG"></div>
                      <div id="fountainG_8" className="fountainG"></div>
                  </div>
              </animated.div>
          )}
            <animated.div scrollTop={props.scroll}>
            <Menu accion={set} state={state} alturas={alturas}/>
            <Inicio      />
            <Profile     />
            <Discografia />
            <Eventos     />
            <SliderFotos />
            <Footer      />
            </animated.div>
      </animated.div>
    )
}

export default Main;