import React, {Component,useState} from "react";
import styled from 'styled-components';
import {Flex, Btn, Sombra} from "./ui";
import {Controller, Scene} from "react-scrollmagic";
import SimpleReactValidator from 'simple-react-validator';
import axios from "axios";
import {useTransition, animated} from 'react-spring';

const Pie = styled.footer`
     width:100%;
     background-image:url(./img/fondofooter.jpg);
     background-position:center;
     background-repeat:no-repeat;  
          background-attachment:fixed;  

     background-size:cover;  
     position:relative;
`

const Contbtn = styled.div`
    transform-style:preserve-3d;
    perspective:600px;
    
    overflow:hidden;
`


const Campo = styled.div`
    width:100%;
    margin-bottom:8px;
    
    input,textarea{
        width:100%;
        background:transparent;
        color:white;
        border:1px solid ${props => props.error ? "red" : "rgba(255,255,255,.5)"};
        padding: .5em .75em;
        cursor:pointer;
        height:45px;
        transition: all 500ms cubic-bezier(0.190, 1.000, 0.220, 1.000);
        
        &:focus{
            border-color:white;
            outline:none;
            background:rgba(0,0,0,.5);
            
        }
        
        &:hover{
        border-color:white;
       
        }
    }
    
    small{color:red;}
    
    textarea{
        height:120px;
        resize:none;
    }
    
    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
      color: rgba(255,255,255,.8);
      opacity: 1; /* Firefox */
      
    }
    
    :-ms-input-placeholder { /* Internet Explorer 10-11 */
      color: rgba(255,255,255,.8);
      
    }
    
    ::-ms-input-placeholder { /* Microsoft Edge */
      color: rgba(255,255,255,.8);
      
    }
`

const Micampo = ({name = "", value = "", placeholder = "", onChange = null, TA, type = "text", error}) => {
    return (
        <Campo error={error !== undefined && error !== ""}>
            {TA ?
                <textarea
                    id=""
                    cols="30"
                    rows="10"
                    onChange={onChange}
                    placeholder={placeholder}
                    name={name}
                    value={value}
                >

                </textarea>
                : <input
                    type={type}
                    name={name}
                    placeholder={placeholder}
                    onChange={onChange}
                    value={value}
                />
            }
            <small>{error}</small>
        </Campo>
    )
}

const CmbiodeBtn = ({loading,enviado}) =>{

   const [toggle,set] = useState(false);

    const transitions = useTransition(enviado, null, {
        from: { opacity: 1,transform:"rotateX(-90deg)"},
        enter: {opacity: 1 , transform:"rotateX(0deg)"},
        leave: {opacity: 0,position:"absolute",top:"0",left:"0",transform:"rotateX(-90deg)"},
    })

    return(
        <Contbtn className="cont-btn col-12 px-0 py-2">

            {transitions.map(({item, key, props}) =>
                item
                    ?
                    <animated.div key={key}  style={{...props,height:"50px",lineHeight:"1em"}} className="cb text-center col-12">
                        <h4 className={"col-12 text-center"} style={{height:"50px",lineHeight:"1em"}}>¡Mensaje enviado!</h4>
                    </animated.div>
                    :
                    <animated.div key={key}  style={props} className="col-12 px-0">
                        <Btn
                            type={"submit"}
                            className={loading ? "loading " : " ro"}
                            disabled={loading}
                        >
                            {loading ? "" : "Enviar"}
                        </Btn>
                    </animated.div>
            )}

        </Contbtn>
    )
}

class Formulario extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: "",
            email: "",
            mensaje: "",
            loading: false,
            enviado:false,
            error:""
        }
        this.validator = new SimpleReactValidator({
            messages: {
                email: 'Ingrese un email válido.',
                required: 'Debe ingresar un :attribute.',
                min: 'El :attribute no es válido'
            },
            validators: {
                email: {
                    message: 'El :attribute no es válido',
                    rule: (val, params, validator) => {
                        var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;
                        return re.test(val);
                    }
                },
                mensaje: {
                    message: 'El :attribute debe contener almenos 10 caracteres',
                    rule: (val, params, validator) => {
                        var min = 10
                        return val.length > min;
                    }
                },

            }
        })
    }

    setValue = (e) => {
        var value = e.target.value;
        var nombre = e.target.name;

        this.setState({[nombre]: value})
    }

    submitForm = (e) => {
        e.preventDefault()
        if (this.validator.allValid()) {
            this.setState({loading: true})

            let data = {
                email: this.state.email,
                nombre: this.state.nombre,
                mensaje: this.state.mensaje,
            }
            const url = "http://diegoalejandro.co/send.php";
            axios.post(url, data)
                .then(response => {
                    if (response.status === 200) {
                        this.setState({loading: false, name: "", email: "", mensaje: "", enviado:true},() => setTimeout(()=> this.setState( {enviado:false}),6000))
                        console.log()
                    }

                    else{
                        this.setState({ error:"Ups, hubo error, intenta de nuevo"},() => setTimeout(()=> this.setState( {error:""}),6000))
                    }
                })

                .catch(() => {
                    this.setState({loading: false, error:"Error en servidor, intente más tarde."},() => setTimeout(()=> this.setState( {error:""}),6000))

                    console.log("Can’t access " + url + " response. Blocked by browser?")
                })

        } else {
            this.validator.showMessages();
            // rerender to show messages for the first time
            // you can use the autoForceUpdate option to do this automatically`
            this.forceUpdate();
        }
    }

    render() {


        return (
            <div className={"col-12 px-0"}>
                <form className={"col-12 px-0"} onSubmit={this.submitForm}>
                    <Micampo
                        placeholder={"Nombre"}
                        className={"pb-2"}
                        name={"name"}
                        value={this.state.name}
                        onChange={(e) => this.setValue(e)}
                        error={this.validator.message('Nombre', this.state.name, 'required|min:3')}
                    />

                    <Micampo
                        placeholder={"Email"}
                        className={"pb-2"}
                        value={this.state.email}
                        name={"email"}
                        onChange={(e) => this.setValue(e)}
                        error={this.validator.message('Email', this.state.email, 'required|email')}
                    />

                    <Micampo
                        TA
                        placeholder={"Mensaje"}
                        className={"pb-2"}
                        name={"mensaje"}
                        value={this.state.mensaje}
                        onChange={(e) => this.setValue(e)}
                        error={this.validator.message('Mensaje', this.state.mensaje, 'required|mensaje')}
                    />

                    <CmbiodeBtn
                        loading={this.state.loading}
                        enviado={this.state.enviado}
                    />
                    <small style={{color:"white"}}>{this.state.error}</small>
                </form>
            </div>
        )
    }
}

const Foto = styled.div`
    background-image:url(${props => props.url});
    background-repeat:no-repeat;
    background-position:center;
    background-color:white;
    background-size:cover;
    padding: 0  3px 3px 0;
    Flex: 0 1 33.33%;
    
    img{
        width:100%;
        height:auto;
        display:block;
        opacity:0;
    }
 `
const Fotoshow = ({url, className}) => {
    return (
        <Foto url={url} className={className}>
            <img src="/img/recuadro.png" alt=""/>
        </Foto>
    )
}

const Footer = () => {

    return (
        <Pie id={"pie"}>

            <div id="sube"></div>
            <Sombra/>

            <Controller>
                <Scene
                    duration={500}
                    classToggle={['.menu', "hideM"]}
                    triggerElement={`#pie`}
                    // indicators={true}
                >
                    <div></div>
                </Scene>
                <Scene
                    duration={500}
                    classToggle={['.subir', "Shows"]}
                    triggerElement={`#sube`}
                    // indicators={true}
                >
                    <div></div>
                </Scene>
            </Controller>
            <Flex alg={"flex-start"} className="col-12 col-md-11 col-lg-10 mx-auto pb-3 pt-5 px-md-0">
                <div className="col-12 col-md-4 px-md-0 pb-3">
                    <h4 className={"cr"}> FACEBOOK</h4>
                    <Flex>
                        <Fotoshow className={"col-4 px-0"}/>
                        <Fotoshow className={"col-4 px-0"}/>
                        <Fotoshow className={"col-4 px-0"}/>
                        <Fotoshow className={"col-4 px-0"}/>
                        <Fotoshow className={"col-4 px-0"}/>
                        <Fotoshow className={"col-4 px-0"}/>
                    </Flex>
                </div>
                <div className="col-12 col-md-4 pl-md-4 pb-4">
                    <h4 className={"cr"}>MANAGER/CONTRATACIONES</h4>
                    <p className={"cb"}>
                        Diana Gonzalez <br/>
                        57-314 770 5102 <br/>
                        contacto@diegoalejandro.co <br/>
                        Colombia. <br/>
                    </p>
                    <h4 className={"cr"}>Sígueme</h4>
                    <Flex jc={"flex-start"}>
                        <a href="https://www.facebook.com/Diego-Alejandro-Tenor-Colombiano-181630645287855/"
                           target={"_blank"} className={"d-block mr-1 link-red"}><img src="/img/facebook.png" alt=""
                                                                                      width={"30px"}
                                                                                      height={"auto"}/></a>
                        <a href="https://www.youtube.com/user/departamentodeprensa" target={"_blank"}
                           className={"d-block mr-1 link-red"}><img src="/img/youtube.png" alt="" width={"30px"}
                                                                    height={"auto"}/></a>
                        <a href="https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2Fdiego_alejandromusic%3Figshid%3D1u9dqw89fhlj7%26fbclid%3DIwAR3ZvXMNCxaqtzLLarw4dZ2QvZAYpBRJiZeAi9ejrZyACGj3Au0cAsFHQbE&h=AT1Z-Dx7euWfbBVkHnLVua7PEckA6NZUiOgTbneLxxxRHkZ83EYkNcDYfpxHdd7XhcHL31Xrf1iezyTYoW-3g0Tn2FuGYLqywRKMeuVtO8q0iU92aL-PbJeDLGLm1pwWzbICac96HzMJEuFNyENa7mcl"
                           target={"_blank"} className={"d-block mr-1 link-red"}><img src="/img/instagram.png" alt=""
                                                                                      width={"30px"}
                                                                                      height={"auto"}/></a>
                    </Flex>

                </div>
                <div className="col-12 col-md-4 px-md-0">
                    <h4 className={"cr"}>ESCRÍBEME</h4>
                    <Formulario/>
                </div>
            </Flex>
            <Flex className="col-12 col-md-11 col-lg-10 mx-auto py-3 px-md-0">
                <img src={"/img/metodos/epayco.png"} className={"px-3 mb-2"} height={"25px"} width={"auto"} alt={""}/>
                <img src={"/img/metodos/visa.png"} className={"px-3 mb-2"} height={"25px"} width={"auto"} alt={""}/>
                <img src={"/img/metodos/mastercard.png"} className={"px-3 mb-2"} height={"25px"} width={"auto"}
                     alt={""}/>
                <img src={"/img/metodos/american.png"} className={"px-3 mb-2"} height={"25px"} width={"auto"} alt={""}/>
                <img src={"/img/metodos/dinners.png"} className={"px-3 mb-2"} height={"25px"} width={"auto"} alt={""}/>
                <img src={"/img/metodos/pse.png"} className={"px-3 mb-2"} height={"25px"} width={"auto"} alt={""}/>
                <img src={"/img/metodos/safetypay.png"} className={"px-3 mb-2"} height={"25px"} width={"auto"}
                     alt={""}/>
            </Flex>

            <Flex className="col-12 col-md-11 col-lg-10 mx-auto py-3 px-md-0" jc={"space-between"}>
                <div className="col-12 col-md-5 px-md-0 pb-3 pb-md-0">
                    <small className={"cb"}><span className={"cr"}>© 2019 Diego Alejandro.</span> Todos los derechos
                        reservados
                    </small>
                </div>
                <div className="col-12 col-md-5 text-md-right px-md-0">
                    <small className={"cb"}>Design with ❤ by <a href="https://kitchenpx.com/" target={"_blank"}
                                                                style={{color: "#fce726"}}>Kitchenpixels</a></small>
                </div>
            </Flex>
        </Pie>
    )
}

export default Footer;