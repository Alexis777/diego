import React from "react";
import styled from "styled-components";
import {Flex} from "./ui";


const Iniciocont = styled.section`
     width:100%;
     position:relative;
     bacgkground-color:black;
     background-image:url(./img/inicio.jpg);
     background-position:top right;
     background-repeat:no-repeat;  
     background-size:cover;
     background-attachment:fixed;  
     height:auto;
     z-index:0;
     
     .firma{
        width:100%;
     }
     
     @media all and (min-width:768px){
     
     height:100vh;
     
     }
     
      @media all and (max-width:520px){
      text-align:center;
      
      .firma{
        width:60%;
        margin:auto;
        max-width:350px;
     }
      }
`

const TextoI = styled(Flex)`
    height:100%;
    min-height:100vh;
    align-items:flex-start;
    flex-flow:column wrap;

    
`

const Inicio = () =>{
    return(
        <Iniciocont id={"01"}>
            <TextoI className={" col-md-10 mx-auto "}>
               <div className="" style={{width:"100%"}}>
                   <div className="col-11 .col-md-6 col-lg-5 px-0 mx-auto mx-md-0 ">

                      <img src={"/img/firma.png"} className={"firma "} alt={"Diego firma"}/>

                       <p className={"cb text-center text-md-left"}>
                           “Tengo la profesión más bella del mundo, el arte decidió conquistarme y he dedicado mi vida a entregarme a ella.”

                       </p>
                   </div>
               </div>
            </TextoI>
        </Iniciocont>
    )
}

export default Inicio;