import styled from "styled-components";


export const Colors = {
    blanco:"white",
    gris:"#999999",
    negro:"#444444",
    rojo:"#D65A31"
}

export const Flex = styled.div`
    display:flex;
    flex-flow: ${props => props.column ? "column wrap" : "row wrap"};
    justify-content: ${props => props.jc ? props.jc : "center"};
    align-items: ${props => props.alg ? props.alg : "center"};
    flex: ${props => props.flex ? props.flex : "0 0 100%" }
`

export const Elastic = styled.div`
    flex:${props => props.flex? props.flex : "0 0 auto"}
`

export const Btn = styled.button`
    width:100%;
    height:50px;
    color:white;
    cursor:pointer;
    text-align:center;
    font-family: 'Staatliches', cursive;
    background:${Colors.rojo};
    border:none;
    font-size:24px;
    lettering-spacing:2px;
    line-height:45px;
    transition: all 500ms cubic-bezier(0.190, 1.000, 0.220, 1.000);
    
    &:focus{
        outline:none;
    }
    
    &:hover{
     background:${Colors.blanco};
     color:${Colors.rojo};
    }
    
    &:disabled{
        opacity:.8;
    }
    
    
`

export const Sombra = styled.div`
    position:absolute;
    top:0;
    left:0;
    width:100%;
    height:100%;
    background:rgba(0,0,0,${props => props.alpha ? props.alpha : ".8"});
`
