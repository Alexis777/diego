import React,{useState} from "react";
import styled from "styled-components";
import {Flex, Sombra} from "./ui";
import MyPlayer from "./player";



const Discocont = styled.section`
     width:100%;
     background-image:url(./img/fondobody.jpg);
     background-position:center;
     background-repeat:no-repeat;  
     background-size:cover;
     background-attachment:fixed;  
     z-index:0;
     position:relative;
`


const Album = ({titulo, orden, caratula,song,reproduciendo,Setsong}) => {
    return (
        <div className={"col-12 px-0"}>
            <MyPlayer
                titulo={titulo}
                caratula={caratula}
                song={song}
                reproduciendo={reproduciendo}
                SetSong={Setsong}
                orden={orden}
            />
        </div>

    )

}

const Discografia = ({altura}) => {

        const [reproduciendo,setSong] = useState("");

    return (

        <Discocont id={"discos"} className={"pb-5"}>

            <Sombra aplpha={".7"}/>
            <div className="col-12 col-md-8 mx-auto">
                <Flex className={"px-0 cont-discos  position-relative col-12 "} jc={"space-around"}>
                    <div className="col-12 px-0 text-right cont-discos">
                        <h1 className={"cr text-center"}>DISCOGRAFÍA</h1>
                    </div>

                    <div className="col-6 col-md-5 col-lg-4  px-0">
                        <Album titulo={"Hoy que te vas"}
                               caratula={"01.jpg"}
                               altura={altura}
                               ancla={"d01"}
                               song={["/mp3/Hoy_que_te_vas.mp3","/mp3/Hoy_que_te_vas.ogg","/mp3/Hoy_que_te_vas.webm"]}
                               reproduciendo={reproduciendo}
                               Setsong={setSong}
                               orden={"primero"}
                        />
                    </div>
                    <div className="col-6 col-md-5 col-lg-4  px-0">
                        <Album titulo={"Hoy te diré"}
                               caratula={"02.jpg"}
                               altura={altura}
                               ancla={"d02"}
                               song={["/mp3/Hoy_te_dire.mp3","/mp3/Hoy_te_dire.ogg","/mp3/Hoy_te_dire.webm"]}
                               reproduciendo={reproduciendo}
                               Setsong={setSong}
                               orden={"segundo"}

                        />
                    </div>
                    <div className="col-12"></div>
                    <div className="col-6 col-md-5 col-lg-4  px-0">
                        <Album titulo={"Me encantas"}
                               caratula={"03.jpg"}
                               altura={altura}
                               ancla={"d03"}
                               song={["/mp3/Me_encantas.mp3","/mp3/Me_encantas.ogg","/mp3/Me_encantas.webm"]}
                               reproduciendo={reproduciendo}
                               Setsong={setSong}
                               orden={"tercero"}


                        />
                    </div>
                    <div className="col-6 col-md-5 col-lg-4  px-0">
                        <Album titulo={"Si tu no serás"}
                               caratula={"04.jpg"}
                               altura={altura}
                               ancla={"d04"}
                               song={["/mp3/Si_tu_no_seras.mp3","/mp3/Si_tu_no_seras.ogg","/mp3/Si_tu_no_seras.webm"]}
                               reproduciendo={reproduciendo}
                               Setsong={setSong}
                               orden={"cuarto"}

                        />
                    </div>

                </Flex>
            </div>

        </Discocont>


    )
}

export default Discografia;