import React from "react";
import {Flex,Sombra,Colors} from "./ui";
import styled from 'styled-components';


const Eventocont = styled.section`
     width:100%;
     background-image:url(./img/fondoconcierto.jpg);
     background-position:center;
     background-repeat:no-repeat;  
     background-size:cover;  
     background-attachment:fixed;  

     position:relative;
`

const Evento = styled.div`
    
    .fecha{
    background:${Colors.rojo};
    text-align:center
        
        .dia{
            font-size:5em;
            line-height:1em;
            color:rgba(255,255,255,.8);
            
        }
        .mes{
            font-sise:18px;
            color:white;
            line-height:1em;
        }
    }
    .ubicacion{
        background:rgba(0,0,0,.5);
        
        .lugar{
            text-transform:uppercase;
            font-family: 'Staatliches', cursive;
            font-size:1.5em;
            letter-spacing:1px;
        }
    }
    
    @media all and (max-width:480px){
      .fecha{
        .dia{
            font-size:3.5em; 
        }
      }
    }
`

const Eventocard = ({className,fecha,dian,dia,mes,lugar,hora,ciudad})=>{
    return(
        <Evento className={"col-12 px-0"} >
            <Flex alg={"stretch"}>
                <Flex column className="col-4 fecha py-3">
                    <h2 className={"mb-0 dia"}>{dian}</h2>
                    <p className={"mes mb-0"}>{mes}, {dia}</p>
                </Flex>
                <Flex column alg={"flex-start"} className="col-8 ubicacion py-3 px-4">
                    <p className={"mb-0 cb"}>{ciudad}</p>
                    <p className={"mb-0 cb lugar"}>{lugar}</p>
                    <p className={"cr mb-0"}>{hora}</p>
                </Flex>
            </Flex>
        </Evento>
    )
}

const Eventos = () => {
    return(
        <Eventocont className={"py-5 ru"} id={"04"}>
            <Sombra alpha={".5"}/>

            <Flex className="col-12 col-md-10 col-lg-8 mx-auto py-5" >
                <div className="col-12 px-0 py-4">
                    <h1 className={"cb"}>EVENTOS</h1>
                </div>
                <div className="col-12 col-md-6 mb-3">
                    <Eventocard
                        dia={"ssab"}
                        dian={"22"}
                        mes={"Mayo"}
                        ciudad={"Medellin, Antioquia"}
                        lugar={"Plaza mayor"}
                        hora={"8:00pm"}
                    />
                </div>

                <div className="col-12 col-md-6 mb-3">
                    <Eventocard
                        dia={"ssab"}
                        dian={"22"}
                        mes={"Mayo"}
                        ciudad={"Medellin, Antioquia"}
                        lugar={"Plaza mayor"}
                        hora={"8:00pm"}
                    />
                </div>
                <div className="col-12 col-md-6 mb-3">
                    <Eventocard
                        dia={"ssab"}
                        dian={"22"}
                        mes={"Mayo"}
                        ciudad={"Medellin, Antioquia"}
                        lugar={"Plaza mayor"}
                        hora={"8:00pm"}
                    />
                </div>
                <div className="col-12 col-md-6 mb-3">
                    <Eventocard
                        dia={"ssab"}
                        dian={"22"}
                        mes={"Mayo"}
                        ciudad={"Medellin, Antioquia"}
                        lugar={"Plaza mayor"}
                        hora={"8:00pm"}
                    />
                </div>

            </Flex>
            <div className="col-12 py-5 text-center">
                <p className={"mb-0 cb"}>No te pierdas mis próximos conciertos, entérate acá de los lugares y fechas de mis eventos. </p>
            </div>
        </Eventocont>
    )
}

export default Eventos;