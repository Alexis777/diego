import React from "react";
import Slider from "react-slick";
import styled from 'styled-components';
import {Colors} from "./ui";


const Slide = styled.div` 
    height:auto;
    background: white url(/img/${props => props.bg}) no-repeat center;
    background-size:auto 100%;
    
    img{
        width:100%;
        height:auto;
        display:block;
        opacity:0;
    }
`

const Myarrow = styled.button`
    width:45px;
    height:45px;
    border-radius:50%;
    border:2px solid ${Colors.rojo};
    width:45px;
    cursor:pinter;
    background:rgba(0,0,0,.8);
    
    position:absolute;
    top:90%;
    left  : ${props => props.right ? "auto": "40%"};
    right : ${props => props.right ? "40%" : "auto"};
    z-index:2;
   
    
    &:focus{outline:none;}
    &:hover{background:white;}
    
    img{
        width:100%;
        height:auto;
        display:block;
    }
    
   @media all and (max-width:600px){
      left  : ${props => props.right ? "auto": "10%"};
    right : ${props => props.right ? "10%" : "auto"};
   }
`


const Arrow = ({right,onClick})=>{
    return(
        <Myarrow right={right} onClick={onClick}>
            <img src={`/img/${right ?"right-arrow":"left-arrow"}.png`} alt=""/>
        </Myarrow>
    )
}



const SliderFotos = ({Imagenes})=>{

    const settings = {
        infinite: true,
        autoplay:true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        nextArrow: <Arrow right/>,
        prevArrow: <Arrow />,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]

    };

    return(
        <div className={"co col-12 pb-5"} id={"05"}>
           <div className="col-12 px-md-0 text-right col-md-8 mx-auto px-md-auto py-4"> <h1 className={"cr"}> GALERÍA</h1></div>

            <Slider {...settings}>
                <Slide ><img src="/img/recuadro.png" alt=""/></Slide>
                <Slide ><img src="/img/recuadro.png" alt=""/></Slide>
                <Slide ><img src="/img/recuadro.png" alt=""/></Slide>
                <Slide ><img src="/img/recuadro.png" alt=""/></Slide>
            </Slider>
        </div>
    )
}

export default SliderFotos;
// {Imagenes.length > 0 &&
{/*{Imagenes.map((item,index )=>  <Slide key={index}> <img src={item}  alt="" width={"100%"} height={"auto"}/></Slide>)}*/}
