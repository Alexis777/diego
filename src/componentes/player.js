import React,{Component} from "react";

import {Flex, Colors} from "./ui";
import styled from "styled-components";
import ReactPlayer from "react-player";


const ContPlayer = styled.section`
    background: rgba(0,0,0,.8); 
    box-shadow:${props => props.active ? "0 0 20px " + Colors.rojo : "none" }
    transition: all 500ms cubic-bezier(0.040, 0.830, 0.185, 1.015);
    transform:scale(${props => props.active ? ".9" : "1"});
    
   .time{
    color:${Colors.rojo};
    line-height:0;
    text-align:right;
    width:100%;
   }
   
   p{position:relative; z-index:22;}
`

const Barra = styled.div`
    margin-top:5px;
    background:#444444;
    height:5px;
    border-radius:2px;
    width:100%;
    color${Colors.rojo}!important;
    overflow:hidden;
    position:relative;
    z-index:33;
    
    .color{
        background:${Colors.rojo};
        height:100%;
        margin:0;
        width:${props => props.width ? props.width + "%" : "0"};
    }
`

const Btnplayer = styled.button`
    height100px;
    width:100px;
    cursor:pointer;
    position:relative;
    
    &:focus{
        outline:none;
    }
    
    img{width:100%; height:auto; display:block;position:relative; z-index:3;margin:2px 0 0;}
    
    &:after{
       border-radius:50%;
        content:'';
        position:absolute;
        width:100px;
        height:100px;
        background:${Colors.rojo};
        opacity:0;
        top:50%;
        left:50%;
        transform:translate(-50%,-50%) scale(0);
        transition: all 500ms cubic-bezier(0.190, 1.000, 0.220, 1.000);
    }
    
    &:hover{
        &:after{
        opacity:1;
        transform:translate(-50%,-50%) scale(.9);    
       }
    }
`

const Caratula = styled.div`
   
     background-image:url(./img/${props => props.album});
     background-position:center;
     background-repeat:no-repeat;  
     background-size:cover;
     box-shadow:0 5px 20px rgba(0,0,0,1);

     
     img{
        opacity:0;
        width:100%;
        height:auto;
        display:block;
     }  
`

const PlayPause = styled.div`
    position:absolute;
    top:0;
    left:0;
    width:100%;
    height:100%;
    background:${props => props.activo ? "rgba(0,0,0,.5)" : "none"};
    transition: all 500ms cubic-bezier(0.040, 0.830, 0.185, 1.015);

    
    &:hover{
    background:rgba(0,0,0,.5);
    }
    
    .cont{
        height:100%;
    }
    
    .stop{
        position:absolute;
        bottom:1em;
        left:calc(50% - 25px); 
        background:black;
        border-radius:25px;
        width:40px;
        height:40px;
        padding:3px;
        
        img{width:100%;height:100%; display:block;}
        
          &:after{
       
        width:40px;
        height:40px;
     
    }   
    
    }
    
`

class  MyPlayer extends Component{

    state = {
       url:[],
       playing:false,
       progress:"",
       duracion:0,
       loaded:0
    }


     Progress = (prog) => {

         var sec = (parseInt(prog.playedSeconds) * 60) % 60;

        var segundos = sec;
        var minutos  =  parseInt(Math.round(prog.playedSeconds) / 60);
        this.setState({progress: `0${minutos}:${segundos}`,loaded: parseInt(prog.playedSeconds)})

    }


    componentDidUpdate(prevProps, prevState, snapshot) {

        if (prevProps.reproduciendo !== this.props.reproduciendo ) {
            if(this.props.reproduciendo === this.props.orden){
                return this.setState({url:[this.props.song]});
            }else{
                return this.setState({playing:false},()=>this.stop());
            }
        }
    }

    componentDidMount() {
        this.setState({url:[this.props.song]})
    }

    start = () => {

        this._audio.play();


    }

    stop = () => {

        this._audio.pause();


    }


    render(){

    return(
        <ContPlayer className={" mb-3 p-2"} active={this.state.playing} >
            <p className={"cb mb-0 text-center"}>{this.props.titulo}</p>
            <Caratula album={this.props.caratula}><img src="/img/recuadro.png" alt=""/></Caratula>
           <Flex>


               <audio ref={ a => this._audio = a }>
                   <source src={this.props.song[0]} type="audio/mp3" />
                   <source src={this.props.song[1]} type="audio/ogg" />
                   <source src={this.props.song[1]} type="audio/webm" />
               </audio>



               <PlayPause activo={this.state.playing} >
                   <Flex className={"cont "}  column>
                   {!this.state.playing &&
                   <Btnplayer onClick={() =>{
                       this.props.SetSong(this.props.orden);
                       this.setState({playing:true})
                       this.start()}
                   }>
                       <img src="/img/play-button.png" alt=""/>
                   </Btnplayer>
                   }

                   {this.state.playing &&
                   <Btnplayer onClick={() =>{
                       this.setState({playing:false});
                       this.stop();
                       }}>
                       <img src="/img/pause.png" alt=""/>
                   </Btnplayer>
                   }
                   </Flex>

               </PlayPause>

           </Flex>
        </ContPlayer>
    )
    }
}

export default MyPlayer;